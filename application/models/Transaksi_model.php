<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
    }

    private $_table = "transaksi_pemesanan";


	public function TampilDataTransaksi()
	{
	$query = $this->db->query("SELECT A. *, B.nama, C.nama_menu FROM " . $this->_table . " AS A
								inner join master_karyawan as B on A.nik=B.nik
								inner join master_menu as C on A.kode_menu=C.kode_menu");
	return $query->result();
	}
	public function savepemesanan($id)
	{
	    $pegawai			= $this->input->post('nama_pegawai');
        $nama_pelanggan     = $this->input->post('nama_pelanggan');
        $kode_menu          = $this->input->post('nama_menu');
        $qty                = $this->input->post('qty');
        $hargamenu          = $this->menu_model->cariHargaMenu($kode_menu);

    $data['id_pemesanan']	= $id;
    $data['nik']			= $pegawai;
	$data['tgl_pemesanan']	= date('Y-m-d');
	$data['nama_pelanggan']	= $nama_pelanggan;
	$data['kode_menu']		= $kode_menu;
	$data['qty']			= $qty;
	$data['total_harga']   =  $qty * $hargamenu;
	$this->db->insert($this->_table, $data);
	}
}