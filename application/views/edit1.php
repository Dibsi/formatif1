<?php
  foreach ($detail_menu as $data) {
    $kode_menu    = $data->kode_menu;
    $nama_menu    = $data->nama_menu;
    $harga        = $data->harga;
    $keterangan   = $data->keterangan;
  }
?>

<div align="center"><h1>Edit Data Menu</h1></div>
<form method="POST" action="<?=base_url()?>menu/EditMenu/<?= $kode_menu; ?>">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <tr>
    <td>Kode Menu</td>
    <td>:</td>
    <td><input type="text" name="kode_menu" id="kode_menu" value="<?=$kode_menu;?>" maxlength="10"></td>
  </tr>
  <tr>
    <td>Nama Menu</td>
    <td>:</td>
    <td><input type="text" name="nama_menu" id="nama_menu" value="<?=$nama_menu;?>" maxlength="50"></td>
  </tr>
   <tr>
    <td>Harga</td>
    <td>:</td>
    <td><input type="text" name="harga" id="harga" value="<?=$harga;?>" maxlength="30"></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><textarea name="keterangan" id="keterangan" cols="45" rows="5"><?=$keterangan;?></textarea></td>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"></td>
  </tr>
  <td>&nbsp;</td>
    <td>&nbsp;</td>
   <td width="334"><a href="<?=base_url();?>menu/listmenu"><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></td>
   </td>
</table>
</form>