<?php
  foreach ($detail_pegawai as $data) {
    $nik      = $data->nik;
    $nama     = $data->nama;
    $alamat     = $data->alamat;
    $telp     = $data->telp;
    $tempat_lahir = $data->tempat_lahir;
    $tanggal_lahir    = $data->tanggal_lahir;
  }
  $tahun_pisah = substr($tanggal_lahir, 0, 4);
  $bulan_pisah = substr($tanggal_lahir, 5, 2);
  $tanggal_pisah = substr($tanggal_lahir, 8, 2);
?>

<div align="center"><h1>Edit Data Karyawan</h1></div>
<form method="POST" action="<?=base_url()?>pegawai/EditPegawai/<?= $nik; ?>">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td><input type="text" name="nik" id="nik" value="<?=$nik;?>" maxlength="10" readonly></td>
  </tr>
  <tr>
    <td>Nama Pegawai</td>
    <td>:</td>
    <td><input type="text" name="nama_pegawai" id="nama_pegawai" value="<?=$nama;?>" maxlength="50"></td>
  </tr>
   <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?></textarea></td>
  </tr>
   <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=$telp;?>" maxlength="50"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=$tempat_lahir;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    	<?php
       for($tgl=1;$tgl<=31;$tgl++){
      $select_tgl = ($tgl == $tanggal_pisah) ? 'selected' : '';
    ?>
        <option value="<?= $tgl; ?>" <?=$select_tgl;?>>
        <?= $tgl; ?></option>
        <?php
        }
    ?>
    </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$b=0;
    while(each($bulan)){
      if($b+1== $bulan_pisah){
        $n = 'SELECTED';
      }else{
        $n = '';
      }   
    ?>
      <option <?=$n;?> value="<?=$b+1;?>" ><?=$bulan[$b];?></option>
      <?php
        $b++;
    }
    ?>
      </select>
      <select name="thn" id="thn">
      <?php
		for($thn = 1990; $thn <= date('Y');$thn++){
      $select_thn = ($thn == $tahun_pisah) ? 'selected' : '';
    ?>
        <option value="<?=$thn;?>" <?=$select_thn;?>><?=$thn;?></option>
      <?php
    }
    ?>
      </select>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"></td>
  </tr>
  <td>&nbsp;</td>
    <td>&nbsp;</td>
   <td width="334"><a href="<?=base_url();?>pegawai/listpegawai"><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></td>
   </td>
</table>
</form>